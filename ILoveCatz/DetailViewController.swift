//
//  DetailViewController.swift
//  ILoveCatz
//
//  Created by Colin Eberhardt on 19/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
  
  var cat: Cat!

  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var attributionText: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    
    imageView.image = UIImage(named: cat.image)
    attributionText.text = cat.attribution
    title = cat.title
  }

}

