//
//  MasterViewController.swift
//  ILoveCatz
//
//  Created by Colin Eberhardt on 19/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, UIViewControllerTransitioningDelegate, UINavigationControllerDelegate {
    
    private let bounceAnimationController = BouncePresentAnimationController()
    private let shrinkDismissAnimationController = ShrinkDismissAnimationController()
    private let flipAnimationController = FlipAnimationController()
    private let swipeInteractionController = SwipeInteractionController()
    
    private var cats: [Cat] {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        return appDelegate.cats
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let imageView = UIImageView(image: UIImage(named: "cat"))
        navigationItem.titleView = imageView
        navigationController?.delegate = self
        
        //    for fontFamilyName in UIFont.familyNames() {
        //        println("-- \(fontFamilyName) --")
        //
        //        for fontName in UIFont.fontNamesForFamilyName(fontFamilyName as String) {
        //            println(fontName)
        //        }
        //
        //        println(" ")
        //    }
    }
    
    override func viewWillAppear(animated: Bool) {
        animateTable()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            // find the tapped cat
            let indexPath = tableView.indexPathForSelectedRow()
            let cat = cats[indexPath!.row]
            
            // provide this to the detail view
            let detailVC = segue.destinationViewController as DetailViewController
            detailVC.cat = cat
        }
        
        if segue.identifier == "ShowAbout" {
            let toVC = segue.destinationViewController as UIViewController
            toVC.transitioningDelegate = self
        }
    }
    
    func animateTable(){
        tableView.reloadData()
        
        let cells = tableView.visibleCells()
        let tableHeight: CGFloat = tableView.bounds.size.height
        
        for i in cells {
            let cell: UITableViewCell = i as UITableViewCell
            cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        
        var index = 0
        
        for a in cells {
            let cell: UITableViewCell = a as UITableViewCell
            UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: nil, animations: {
                cell.transform = CGAffineTransformMakeTranslation(0, 0)
                }, completion: nil)
            index += 1
        }
    }
    
    
    // MARK: - Table View
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cats.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as UITableViewCell
        cell.textLabel!.font = UIFont(name: "YanoneKaffeesatz-Regular", size: 24);
        cell.textLabel!.text = cats[indexPath.row].title
        
        return cell
    }
    
    // customAnimate
    func customAnimate(animateVeiw view: UIView, duration dur: NSTimeInterval, delay del: NSTimeInterval, springDamping dam: CGFloat, springVelosity vel: CGFloat? = 0) -> Void {
        UIView.animateWithDuration(dur, delay: del, usingSpringWithDamping: dam, initialSpringVelocity: vel!, options: nil, animations: {
            view.transform = CGAffineTransformIdentity
            }, completion: nil)
    }
    
    // MARK: - UIViewControllerTransitioningDelegate
    
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return bounceAnimationController
    }
    
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return shrinkDismissAnimationController
    }
    
    // MARK: - UINavigationControllerDelegate
    func navigationController(navigationController: UINavigationController, animationControllerForOperation operation: UINavigationControllerOperation, fromViewController fromVC: UIViewController, toViewController toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        if operation == .Push {
            swipeInteractionController.wireToViewController(toVC)
        }
        
        flipAnimationController.reverse = operation == .Pop
        return flipAnimationController
    }
    
    func navigationController(navigationController: UINavigationController, interactionControllerForAnimationController animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return swipeInteractionController.interactionInProgress ? swipeInteractionController : nil
    }
}



