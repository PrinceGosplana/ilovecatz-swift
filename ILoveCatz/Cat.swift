//
//  Cat.swift
//  ILoveCatz
//
//  Created by Colin Eberhardt on 19/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import Foundation

class Cat {

  let image: String
  let title: String
  let attribution: String

  init(image: String, title: String, attribution: String) {
    self.image = image
    self.title = title
    self.attribution = attribution
  }

}
