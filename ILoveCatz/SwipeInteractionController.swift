//
//  SwipeInteractionController.swift
//  ILoveCatz
//
//  Created by Александр Исаев on 11.03.15.
//  Copyright (c) 2015 Colin Eberhardt. All rights reserved.
//

import UIKit

class SwipeInteractionController: UIPercentDrivenInteractiveTransition {
    
    private var shouldCompleteTransition = false
    private var navigationController: UINavigationController!
    var interactionInProgress = false
    
    var completionSeed: CGFloat {
        return 1 - percentComplete
    }
    
    func wireToViewController(viewController: UIViewController) {
        navigationController = viewController.navigationController
        prepareGestureRecognizerInView(viewController.view)
    }
    
    private func prepareGestureRecognizerInView(view: UIView) {
        let gesture = UIPanGestureRecognizer(target: self, action: "handleGesture:")
        view.addGestureRecognizer(gesture)
    }

    func handleGesture(gestureRecognizer: UIPanGestureRecognizer) {
    
    let translation = gestureRecognizer.translationInView(gestureRecognizer.view!.superview!)
    
    switch gestureRecognizer.state {
    case .Began:
        // start an interactive transition!
        interactionInProgress = true
        navigationController.popViewControllerAnimated(true)
    case .Changed:
        // compute the current position
        var fraction = (translation.x / 200.0)
        fraction = CGFloat(fminf(fmaxf(Float(fraction), 0.0), 1.0))
        
        // should we complete?
        shouldCompleteTransition = fraction > 0.5
        
        // update the animation
        updateInteractiveTransition(fraction)
            
    case .Ended, .Cancelled:
        // finish or cancel
        interactionInProgress = false
        if !shouldCompleteTransition || gestureRecognizer.state == .Cancelled {
            cancelInteractiveTransition()
        } else {
            finishInteractiveTransition()
        }
            default:
        println("Unsupported state")
        }
    }
}
