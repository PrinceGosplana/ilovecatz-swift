//
//  File.swift
//  ILoveCatz
//
//  Created by Colin Eberhardt on 20/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
  @IBAction func doneButtonPressed(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }
}
