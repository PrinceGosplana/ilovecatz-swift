//
//  BouncePresentAnimationController.swift
//  ILoveCatz
//
//  Created by Александр Исаев on 11.03.15.
//  Copyright (c) 2015 Colin Eberhardt. All rights reserved.
//

import UIKit

class BouncePresentAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 0.5
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        // obtain state from the context
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let finalFrame = transitionContext.finalFrameForViewController(toViewController)
        
        // obtain the container view
        let containerView = transitionContext.containerView()
        
        // set initial state
        let screenBounds = UIScreen.mainScreen().bounds
        toViewController.view.frame = CGRectOffset(finalFrame, 0, screenBounds.size.height)
        
        // add the view
        containerView.addSubview(toViewController.view)
        
        // animate
        let duration = transitionDuration(transitionContext)
        
        UIView.animateWithDuration(duration,
            delay: 0.0,
            usingSpringWithDamping: 0.6,
            initialSpringVelocity: 0.0,
            options: .CurveLinear,
            animations: {
                // set the state to animate to
                fromViewController.view.alpha = 0.5
                toViewController.view.frame = finalFrame
            }, completion: { finished in
                // inform the context of completion
                transitionContext.completeTransition(true)
                fromViewController.view.alpha = 1.0
        })

    }
}
