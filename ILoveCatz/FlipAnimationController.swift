//
//  FlipAnimationController.swift
//  ILoveCatz
//
//  Created by Александр Исаев on 11.03.15.
//  Copyright (c) 2015 Colin Eberhardt. All rights reserved.
//

import UIKit

class FlipAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    var reverse: Bool = false
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 1.0
    }
    
    private func yRotation(angle: Double) -> CATransform3D {
        return CATransform3DMakeRotation(CGFloat(angle), 0.0, 1.0, 0.0)
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        
        let containerView = transitionContext.containerView()
        let toVC = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let fromVC = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        
        let toView = toVC.view
        let fromView = fromVC.view
        containerView.addSubview(toView)
        
        // add a perspective transform
        var transform = CATransform3DIdentity
        transform.m34 = -0.002
        containerView.layer.sublayerTransform = transform
        
        // give both VCs the same start frame
        let initialFrame = transitionContext.initialFrameForViewController(fromVC)
        fromView.frame = initialFrame
        toView.frame = initialFrame
        
        // reverse
        let factor = reverse ? 1.0 : -1.0
        
        // flip the to VC halfway round - hiding it
        toView.layer.transform = yRotation(factor * -M_PI_2)
        
        // animate
        let duration = transitionDuration(transitionContext)
        UIView.animateKeyframesWithDuration(duration,
            delay: 0,
            options: .CalculationModeCubic,
            animations: {
                UIView.addKeyframeWithRelativeStartTime(0.0,
                    relativeDuration: 0.5) {
                        // rotate the from view
                        fromView.layer.transform = self.yRotation(factor * M_PI_2)
                }
                UIView.addKeyframeWithRelativeStartTime(0.5,
                    relativeDuration: 0.5) {
                        // rotate to the view
                        toView.layer.transform = self.yRotation(0.0)
                }
                
                
            }, completion: {
            finished in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
        })
    }
}


