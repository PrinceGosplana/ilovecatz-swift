//
//  AppDelegate.swift
//  ILoveCatz
//
//  Created by Colin Eberhardt on 19/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  let cats = [
    Cat(image: "CatInBin.jpg", title: "Cat in a bin", attribution: "http://www.sxc.hu/photo/1406907"),
    Cat(image: "DancingCat.jpg", title: "Dancing cat", attribution: "http://www.sxc.hu/photo/1378836"),
    Cat(image: "KittensInABasket.jpg", title: "Kittens in a basket", attribution: "http://www.sxc.hu/photo/1178601"),
    Cat(image: "RelaxedCat.jpg", title: "Relaxed cat", attribution: "http://www.sxc.hu/photo/1361582"),
    Cat(image: "VeryYoungKitten.jpg", title: "Very young cat", attribution: "http://www.sxc.hu/photo/235473"),
    Cat(image: "YawningCat.jpg", title: "Yawning cat", attribution: "http://www.sxc.hu/photo/1353556"),
    Cat(image: "CuteKitten.jpg", title: "Cute kitten", attribution: "http://www.sxc.hu/photo/1319510")
  ]


  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {

    // style the navigation bar
    let navColor = UIColor(red: 0.97, green: 0.37, blue: 0.38, alpha: 1.0)
    UINavigationBar.appearance().barTintColor = navColor
    UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor()]
    
    // make the status bar white
    UIApplication.sharedApplication().statusBarStyle = .LightContent
    
    return true
  }

}

