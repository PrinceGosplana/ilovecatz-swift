//
//  ShrinkDismissAnimationController.swift
//  ILoveCatz
//
//  Created by Александр Исаев on 11.03.15.
//  Copyright (c) 2015 Colin Eberhardt. All rights reserved.
//

import UIKit

class ShrinkDismissAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning) -> NSTimeInterval {
        return 0.5
    }
    
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
        let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
        let finalFrame = transitionContext.finalFrameForViewController(toViewController)
        
        let containerView = transitionContext.containerView()
        
        toViewController.view.frame = finalFrame
        toViewController.view.alpha = 0.5
        
        containerView.addSubview(toViewController.view)
        containerView.sendSubviewToBack(toViewController.view)
        
        // determine the intermediate and final frame the from view
        let screenBounds = UIScreen.mainScreen().bounds
        
        let shrunkenFrame = CGRectInset(fromViewController.view.frame,
            fromViewController.view.frame.size.width / 4,
            fromViewController.view.frame.size.height / 4)
        
        let fromFinalFrame = CGRectOffset(shrunkenFrame, 0, screenBounds.size.height)
        
        let duration = transitionDuration(transitionContext)
        
        // create a snapshot
        let intermediateView = fromViewController.view.snapshotViewAfterScreenUpdates(false)
        intermediateView.frame = fromViewController.view.frame
        containerView.addSubview(intermediateView)
        
        // remove the real view
        fromViewController.view.removeFromSuperview()
        
        // animate with keyframes
        UIView.animateKeyframesWithDuration(duration,
            delay: 0,
            options: .CalculationModeCubic,
            animations: {
                
                // keyframe one
                UIView.addKeyframeWithRelativeStartTime(0.0,
                    relativeDuration: 0.5,
                    animations: {
                        intermediateView.frame = shrunkenFrame
                        toViewController.view.alpha = 0.5
                })
                
                // keyframe two
                UIView.addKeyframeWithRelativeStartTime(0.5,
                    relativeDuration: 0.5,
                    animations: {
                        intermediateView.frame = fromFinalFrame
                        toViewController.view.alpha = 1.0
                })
                
            }, completion: {
                finished in
                transitionContext.completeTransition(true)
        })
    }
}
